
## How to set up

1. Clone **this repo**.
2. after u clone this repo u can create init.
3. run this command **npm init -y**.
4. after init already create u can install supertest, mocha, chai, dotenv, mochawesome.
5. run this command **npm i supertest mocha chai dotenv mochawesome**.
6. if u already install, u can go to package.json then copy this scripts to script in the package.json
    "test": "./node_modules/mocha/bin/mocha ./test --recursive --reporter mochawesome --reporter-options reportDir=reports/mochawesome/ --timeout 180000",
    "reports": "open 'reports/mochawesome/mochawesome.html'"

---

## How to set run

1. Go to repo and folder API
2. run this commant **npm run test** // this is for running the script
3. run this command **npm run reports** // this is for look the report
3. run this command **npm run slack** // this is for send to slack