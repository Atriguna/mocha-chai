const assert = require('chai').expect;
const get_user = require('../object/reqest.in/getapi')
const post_user = require('../object/reqest.in/postapi')
const datas = require('../data/data')
const get = require('../testcase/reqest.in/get_user.json')
const post = require('../testcase/reqest.in/post_user.json')

describe('Api from reqest.in', () => {

    describe('get user', () => {
        it(`${get.positive.psitive1}`, async () => {
            const resp = await get_user.getListuseer(datas.pages)
            if(resp.status !==200){
                console.log("Get user : "+resp.status+"||" +resp.body)
            }
            assert(resp.status).to.equal(200)
            assert(resp.body.data[0]).to.have.property('id')
            assert(resp.body.data[0]).to.have.property('email')
            assert(resp.body.data[0]).to.have.property('first_name')
            assert(resp.body.data[0]).to.have.property('last_name')
            assert(resp.body.data[0]).to.have.property('avatar')
        });
    });

    describe('post user', () => {
        it(`${post.positive.psitive1}`, async () => {
            const resp = await post_user.createuser(datas.names,datas.jobs)
            if(resp.status !==201){
                console.log("create user : "+resp.status+"||" +resp.body)
            }
            assert(resp.status).to.equal(201)
            assert(resp.body).to.have.property('name')
            assert(resp.body).to.have.property('job')
            assert(resp.body).to.have.property('id')
            assert(resp.body).to.have.property('createdAt')
        });
    });
});
