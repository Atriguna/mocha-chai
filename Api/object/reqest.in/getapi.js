const supertest = require('supertest');
const env = require('dotenv').config();
const api = supertest(process.env.baseurl)

const getListuseer = (pages) => api.get('/api/users?')
 .set('Content-Type', 'application/json')
 .query({page : pages})

module.exports = {
   getListuseer
}