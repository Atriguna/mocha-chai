const supertest = require('supertest');
const env = require('dotenv').config();
const api = supertest(process.env.baseurl)


const createuser = (names, jobs) => api.post('/api/user')
    .set('Content-Type', 'application/json')
    .send({
        "name": names,
        "job": jobs
    })

    module.exports = {
        createuser
     }
